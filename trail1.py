i = ['','','','','','','','','','','','','',]
i[0] ="""                                                                                     """
i[1] ="""      ___           ___           ___           ___           ___           ___      """
i[2] ="""     /\  \         |\__\         /\  \         /\__\         /\  \         /\__\     """
i[3] ="""    /::\  \        |:|  |        \:\  \       /:/  /        /::\  \       /::|  |    """
i[4] ="""   /:/\:\  \       |:|  |         \:\  \     /:/__/        /:/\:\  \     /:|:|  |    """
i[5] ="""  /::\~\:\  \      |:|__|__       /::\  \   /::\  \ ___   /:/  \:\  \   /:/|:|  |__  """
i[6] =""" /:/\:\ \:\__\     /::::\__\     /:/\:\__\ /:/\:\  /\__\ /:/__/ \:\__\ /:/ |:| /\__\ """
i[7] =""" \/__\:\/:/  /    /:/~~/~       /:/  \/__/ \/__\:\/:/  / \:\  \ /:/  / \/__|:|/:/  / """
i[8] ="""      \::/  /    /:/  /        /:/  /           \::/  /   \:\  /:/  /      |:/:/  /  """
i[9] ="""       \/__/     \/__/         \/__/            /:/  /     \:\/:/  /       |::/  /   """
i[10]="""                                               /:/  /       \::/  /        /:/  /    """
i[11]="""                                               \/__/         \/__/         \/__/     """
i[12]="""                                                                                     """

PORT_NUM = 15055
GLOBAL_MIN_Y = 10
GLOBAL_MIN_X = 50
#screen.addstr(1,2,'Press /ip to enter IP . /nick to change nick')
#screen.addstr(1,2,'Press Return Key [ Enter Key ] to start typing')   
HELP_TEXT = [
             '',
             ' * > Type /ip to enter IP to chat with',
             ' * > Type /rr to reply to the last person in the chat',
             ' * > Type /cmd to execute system commmand and /sendcmd to send the output',
             ' * > Type /sc to execute system command and send the output'
             ' * > Type /nick to change nick',
             ' * > Type /help to see help',
             ' * > Type /clear to clear screen',
             ' * > Type /exit to exit program',
             ' * >',
             ' * > Press Return Key [ ENTER ] to type anything',
                ]

import curses
import signal
import sys
import subprocess
from os import system
import socket
import thread
import threading
import os
import datetime
import re
import getpass
import subprocess
#import locale
#locale.setlocale(locale.LC_ALL, '')
#code = locale.getpreferredencoding()

thread_exit_code = None
x=0
output = ['empty']
server_msg = 'Nothing'
chat_box = ['']
chat_box_spam = ['empty']
cmd_out=[]
server_ip=''
reply_ip = ''
try:
    client_name = str(getpass.getuser())
except Exception, e:
    client_name = 'localhost'

#################

def run_server(screen):
    global server_msg
    global chat_box
    global server_name
    global reply_ip
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        server_socket.bind(("", PORT_NUM))
        server_msg = 'Running Server On Port: '+ str(PORT_NUM)
    except Exception, e:
        server_msg = 'Server Error while Starting'
    while (thread_exit_code==None):
        data, address = server_socket.recvfrom(256)
        reply_ip = address[0]
        a=re.search(r'/n/i/k/',data)
        if a:
            b=re.split(r'/n/i/k/',data)
            #############
            now = datetime.datetime.now()
            chat_now_msg = str(now.strftime('%H:%M:%S'))+'['+str(address[0])+']'+'<'+str(b[1])+'> '+str(b[2])
            chat_box.append(chat_now_msg)
            #################
            if address[0] == '127.0.0.1':
                if data=='q' or data=='Q':
                    break

    server_socket.close()


 ###### Curses ############
def show_help():
    global chat_box
    for line in HELP_TEXT:
        chat_box.append(line)

def ctrcevent(signum,frame):
    global thread_exit_code
    thread_exit_code = 1
    curses.echo()
    curses.endwin()
    sys.exit(0)

def get_parm(prompt_string,screen):
    screen.clear()
    screen.border(0)
    screen.addstr(2,2,prompt_string)
    screen.refresh()
    curses.echo()
    input = screen.getstr(10,10,60)
    return input

def get_nick(prompt_string,screen):
    global client_name
    screen.clear()
    screen.border(0)
    screen.addstr(2,2,prompt_string)
    screen.refresh()
    curses.echo()
    input = screen.getstr(10,10,60)
    client_name = input
    screen.move(2,2)
    screen.clrtoeol()

def get_server_ip(prompt_string,screen):
    global server_ip
    screen.clear()
    screen.border(0)
    screen.addstr(2,2,prompt_string)
    screen.refresh()
    curses.echo()
    input = screen.getstr(10,10,60)
    server_ip = input
    screen.move(2,2)
    screen.clrtoeol()
# def execute_cmd(cmd_string):
#     system("clear")
#     a = system(cmd_string)
#     print ""
#     if a == 0:
#          print "Command executed correctly"
#     else:
#          print "Command terminated with error"
#     raw_input("Press enter")
#     print ""

def execute_cmd(prompt_string,screen):
    global cmd_out
    cmd_out = []
    screen.clear()
    screen.border(0)
    screen.addstr(2,2,prompt_string)
    screen.refresh()
    curses.echo()
    command_string = screen.getstr(10,10,500)
    cmd_out.append('command: '+ command_string)
    chat_box.append('command: '+ command_string)
    cmd = re.split(r' ',command_string)
    try:
        proc = subprocess.Popen(cmd, stdout=subprocess.PIPE)
        out = proc.communicate()[0]
    except Exception, e:
        out = "Error"
    outarry = re.split(r'\n',out)
    for line in outarry:
        cmd_out.append(line)
        chat_box.append(line)
    screen.move(2,2)
    screen.clrtoeol()



def display_line(screen):
    scr_y, scr_x = screen.getmaxyx()
    cur_x=0
    cur_y=scr_y-3
    screen.hline(cur_y,cur_x,'-',scr_x)


def clean_chatbox(screen):
    global chat_box
    global chat_box_spam
    scr_y,scr_x = screen.getmaxyx()
    cur_x=1
    cur_y=4
    chat_box = ['messages']
    chat_box_spam = ['messages']
    for pos_y in range(cur_y,scr_y-3):
        try:
            screen.move(pos_y,cur_x)
            screen.clrtoeol()

        except Exception, e:
            pass
        
def main(screen):
    global x
    global output
    global chat_box
    global chat_box_spam
    global server_ip
    global reply_ip

    client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    screen=curses.initscr()
    curses.start_color()
    curses.use_default_colors()
    if curses.has_colors():
        chat_box.append('has colors')
        curses.init_pair(1, curses.COLOR_RED, curses.COLOR_WHITE)
    else:
        chat_box.append('no colors')
    
    

    screen.refresh()
    screen.clear()
    thread_server = thread.start_new_thread(run_server,(screen,))
    #thread_update_chatbox = thread.start_new_thread(display_chatbox,(screen,))
    i_y=4
    i_x=2
    max_y,max_x = screen.getmaxyx()
    
    if max_x > 70:
        for line in i:
            screen.addstr(i_y,i_x,line,curses.color_pair(1))
            i_y=i_y+1
    show_help()
    
    while 1:
        signal.signal(signal.SIGINT,ctrcevent)
        display_line(screen)
        screen.border(0)
        screen.addstr(0,2,client_name,curses.A_BOLD)
        #screen.addstr(1,2,'sending to: ' +str(server_ip)+' server info: '+server_msg + ' \ ',curses.color_pair(1) )
        screen.addstr(1,2,'sending to: ' +str(server_ip)+' server info: '+server_msg + ' \ ',curses.A_DIM )

        

        max_y,max_x = screen.getmaxyx()
        if max_y < GLOBAL_MIN_Y:
            sys.exit('Min Height of window is 10 lines to override open source and edit variables')
        if max_x < GLOBAL_MIN_X:
            sys.exit('Min width of window not satisfied. to override open source and edit variables')
        cur_x = 1
        cur_y = 1
        
           
        chat_order = chat_box[::-1]
        chat_y = max_y - 4

        for line in chat_order:
            if(chat_y > 2):
                screen.move(chat_y,2)
                screen.clrtoeol()
                screen.addstr(chat_y,2,line)
                chat_y = chat_y - 1
            else:
                break
        
        if len(chat_box) > max_y:
            chat_box_spam = ['empty']
            
            chat_box = chat_box[::-1]
            chat_box = chat_box[:max_y-(6+1)]
            chat_box = chat_box[::-1]

        screen.nodelay(1)   
        #input_command = screen.getstr(max_y-2,2,60)
        curses.noecho()
        curses.curs_set(0)
        input_cmd=screen.getch()
        screen.nodelay(0)
##############################################################################################
#############################################################################################
        if input_cmd==ord('\n'):
            curses.echo()
            curses.curs_set(2)
            screen.move(max_y-2,2)
            screen.addstr(max_y-2,2,'Type: ')
            input_command = screen.getstr(max_y-2,8,255)
           
            if input_command == '/ip':
                get_server_ip('enter IP: ',screen)
                input_command = ''

            elif input_command == '/clear':
                clean_chatbox(screen)
                input_command = ''

            elif input_command == '/nick':
                get_nick('enter new nick: ',screen)
                input_command =''
            
            elif input_command == '/help':
                show_help()
                input_command =''

            elif input_command == '/cmd':
                execute_cmd('Enter Command: ',screen)
                input_command =''

            elif input_command == '/intro':
                show_help()
                try:
 
                    chat_box = chat_box[::-1]
                    chat_box = chat_box[:max_y-(len(i)+6+1)]
                    chat_box = chat_box[::-1]

                except Exception, e:
                    pass
                i_y=4
                i_x=2
                for line in i:
                    screen.addstr(i_y,i_x,line,curses.color_pair(1))
                    i_y=i_y+1

                
                input_command=''
            elif input_command == '/rr':
                server_ip = reply_ip
                input_command=''
            elif input_command == '/sendcmd':
                if cmd_out != []:
                    for line in cmd_out:
                        message = '/n/i/k/'+client_name+'/n/i/k/'+line
                        try:
                            client_socket.sendto(message, (server_ip,PORT_NUM))
                        except Exception, e:
                            chat_box.append('ERROR: on message unable to send: '+input_command)
                            chat_box.append('ERROR: server ip: '+server_ip)
                
                input_command =''

            elif input_command == '/sc':
                execute_cmd('Enter Command: ',screen)
                if cmd_out != []:
                    for line in cmd_out:
                        message = '/n/i/k/'+client_name+'/n/i/k/'+line
                        try:
                            client_socket.sendto(message, (server_ip,PORT_NUM))
                        except Exception, e:
                            chat_box.append('ERROR: on message unable to send: '+input_command)
                            chat_box.append('ERROR: server ip: '+server_ip)
                
                input_command =''


            elif input_command == '/exit':
                sys.exit(0)
            
            screen.border(0)
            screen.move(max_y-2,2)
            screen.clrtoeol()
            now = datetime.datetime.now()
            if input_command:
                chat_now_msg = str(now.strftime('%H:%M:%S'))+'[127.0.0.1]'+'<'+client_name+'> '+input_command
                if chat_box_spam[len(chat_box_spam)-1] == input_command:
                    chat_box_spam.append(input_command)
                else:
                    chat_box_spam.append(input_command)
                    chat_box.append(chat_now_msg)
                    message = '/n/i/k/'+client_name+'/n/i/k/'+input_command
                    try:
                        client_socket.sendto(message, (server_ip,PORT_NUM))
                    except Exception, e:
                        chat_box.append('ERROR: on message unable to send: '+input_command)
                        chat_box.append('ERROR: server ip: '+server_ip)
                    

    

if __name__=='__main__':
    curses.wrapper(main)



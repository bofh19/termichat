#video_server.py udp
import cv
import socket
global client_list
import numpy as np
import pickle
def cv2array(im):
    depth2dtype = {
        cv.IPL_DEPTH_8U: 'uint8',
        cv.IPL_DEPTH_8S: 'int8',
        cv.IPL_DEPTH_16U: 'uint16',
        cv.IPL_DEPTH_16S: 'int16',
        cv.IPL_DEPTH_32S: 'int32',
        cv.IPL_DEPTH_32F: 'float32',
        cv.IPL_DEPTH_64F: 'float64',
    }

    arrdtype=im.depth
    a = np.fromstring(
         im.tostring(),
         dtype=depth2dtype[im.depth],
         count=im.width*im.height*im.nChannels)
    a.shape = (im.height,im.width,im.nChannels)
    return a

def array2cv(a):


    dtype2depth = {
            'uint8':   cv.IPL_DEPTH_8U,
            'int8':    cv.IPL_DEPTH_8S,
            'uint16':  cv.IPL_DEPTH_16U,
            'int16':   cv.IPL_DEPTH_16S,
            'int32':   cv.IPL_DEPTH_32S,
            'float32': cv.IPL_DEPTH_32F,
            'float64': cv.IPL_DEPTH_64F,
    }
    try:
        nChannels = a.shape[2]
    except:
        nChannels = 1
    
    cv_im = cv.CreateImageHeader((a.shape[1],a.shape[0]),
              dtype2depth[str(a.dtype)],
              nChannels)
    cv.SetData(cv_im, a.tostring(),
                 a.dtype.itemsize*nChannels*a.shape[1])
    return cv_im


server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.bind(("", 15055))
server_socket.listen(5)

print"UDPServer Waiting for client on port 15055"
x=0
framenum=0
im=''
data=''
a=''
imz=''
#server_socket.setblocking(0)
cv.NamedWindow("w2", 1)
c = cv.WaitKey(10)
while 1:
    client_socket, address = server_socket.accept()
    print 'connected'
    while 1:
        data = client_socket.recv(1640000)
        a=a+data
        print len(a)
        try:
            l=pickle.loads(a)
            a=''
            im=''
            try:
                im=array2cv(l)
                imz=im
            except Exception, e:
                print 'failed to convert'
        except Exception, e:
            pass

        if imz!='':
            cv.ShowImage('w2',imz)
            cv.WaitKey(10)
	
server_socket.close()   
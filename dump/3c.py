import curses
myscreen=curses.initscr()
pad = curses.newpad(100, 100)
#  These loops fill the pad with letters; this is
# explained in the next section
z=0
while z != ord('4'):
  
  for y in range(0, 20):
	  for x in range(0, 20):
		      try: 
			  myscreen.addch(y,x, ord('a') + (x*x+y*y) % 26 )
		      except curses.error: 
			  pass
  z=myscreen.getch()   	  
  #  Displays a section of the pad in the middle of the screen
  pad.refresh( 0,0, 5,5, 20,75)
curses.endwin()
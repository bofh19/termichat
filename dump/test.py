import curses
import signal
import sys
import subprocess
from os import system
import socket
import thread
import threading
import os
import datetime
import re
import getpass
#import locale
#locale.setlocale(locale.LC_ALL, '')
#code = locale.getpreferredencoding()
thread_exit_code = None
x=0
output = ['empty']
server_msg = 'Nothing'
chat_box = ['messages']
chat_box_spam = ['empty']
server_ip=''
try:
    client_name = str(getpass.getuser())
except Exception, e:
    client_name = 'localhost'

#################

def run_server(screen):
    global server_msg
    global chat_box
    global server_name
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        server_socket.bind(("", 5055))
        server_msg = 'Running Server On Port 5055'
    except Exception, e:
        server_msg = 'Server Error while Starting'
    while (thread_exit_code==None):
        data, address = server_socket.recvfrom(256)
        a=re.search(r'/n/i/k/',data)
        
        if a:
            b=re.split(r'/n/i/k/',data)
            #############
            now = datetime.datetime.now()
            chat_now_msg = str(now.strftime('%H:%M:%S'))+' <'+str(b[1])+'> '+str(b[2])
            chat_box.append(chat_now_msg)
            #################
            if address[0] == '127.0.0.1':
                if data=='q' or data=='Q':
                    break

    server_socket.close()


 ###### Curses ############


def ctrcevent(signum,frame):
    global thread_exit_code
    thread_exit_code = 1
    curses.echo()
    curses.endwin()
    sys.exit(0)

def get_parm(prompt_string,screen):
    screen.clear()
    screen.border(0)
    screen.addstr(2,2,prompt_string)
    screen.refresh()
    curses.echo()
    input = screen.getstr(10,10,60)
    return input

def get_nick(prompt_string,screen):
    global client_name
    screen.clear()
    screen.border(0)
    screen.addstr(2,2,prompt_string)
    screen.refresh()
    curses.echo()
    input = screen.getstr(10,10,60)
    client_name = input

def get_server_ip(prompt_string,screen):
    global server_ip
    screen.clear()
    screen.border(0)
    screen.addstr(2,2,prompt_string)
    screen.refresh()
    curses.echo()
    input = screen.getstr(10,10,60)
    server_ip = input

def execute_cmd(cmd_string):
    system("clear")
    a = system(cmd_string)
    print ""
    if a == 0:
         print "Command executed correctly"
    else:
         print "Command terminated with error"
    raw_input("Press enter")
    print ""
    
def display_cursor(screen):
    scr_y, scr_x = screen.getmaxyx()
    cur_x=0
    cur_y=scr_y-3
    screen.hline(cur_y,cur_x,'-',scr_x)
    #while (cur_x<scr_x):
    #    screen.addch(cur_y,cur_x,'-')
    #    cur_x = cur_x + 1

def clean_chatbox(screen):
    global chat_box
    global chat_box_spam
    scr_y,scr_x = screen.getmaxyx()
    cur_x=1
    cur_y=4
    chat_box = ['messages']
    chat_box_spam = ['messages']
    for pos_y in range(cur_y,scr_y-3):
        try:
            screen.move(pos_y,cur_x)
            screen.clrtoeol()

        except Exception, e:
            pass
        
def main(screen):
    global x
    global output
    global chat_box
    global chat_box_spam
    global server_ip

    client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    screen=curses.initscr()
    #curses.start_color()
    screen.clear()
    thread_server = thread.start_new_thread(run_server,(screen,))
    #thread_update_chatbox = thread.start_new_thread(display_chatbox,(screen,))
    while 1:
        signal.signal(signal.SIGINT,ctrcevent)
        display_cursor(screen)
        screen.border(0)
        screen.addstr(0,2,client_name+' Press Ctrl+C to exit')
        screen.addstr(1,2,'Press /ip to enter IP . /nick to change nick')
        screen.addstr(2,2,'sending to: ' +str(server_ip)+' server info: '+server_msg)
        screen.addstr(3,2,'Press Return Key [ Enter Key ] to start typing')
        max_y,max_x = screen.getmaxyx()
        cur_x = 1
        cur_y = 1
        
           
        chat_order = chat_box[::-1]
        chat_y = max_y - 4

        for line in chat_order:
            if(chat_y > 4):
                screen.move(chat_y,2)
                screen.clrtoeol()
                screen.addstr(chat_y,2,line)
                chat_y = chat_y - 1
            else:
                break
        
        
        screen.nodelay(1)   
        #input_command = screen.getstr(max_y-2,2,60)
        curses.noecho()
        curses.curs_set(0)
        input_cmd=screen.getch()
        screen.nodelay(0)
        if input_cmd==ord('\n'):
            curses.echo()
            curses.curs_set(1)
            screen.move(max_y-2,2)
            input_command = screen.getstr(max_y-2,2,60)
           
            if input_command == '/ip':
                cmd_string=get_server_ip('enter IP: ',screen)
            if input_command == '/clear':
                clean_chatbox(screen)

            if input_command == '/nick':
                get_nick('enter new nick: ',screen)

            screen.move(max_y-2,2)
            screen.clrtoeol()
            now = datetime.datetime.now()
            chat_now_msg = str(now.strftime('%H:%M:%S'))+' <'+client_name+'> '+input_command
            if chat_box_spam[len(chat_box_spam)-1] == input_command:
                chat_box_spam.append(input_command)
            else:
                chat_box_spam.append(input_command)
                chat_box.append(chat_now_msg)
                message = '/n/i/k/'+client_name+'/n/i/k/'+input_command
                client_socket.sendto(message, (server_ip,5055))

    

if __name__=='__main__':
    curses.wrapper(main
#video_server.py udp
import cv
import socket
global client_list
import numpy as np
import pickle
def cv2array(im):
    depth2dtype = {
        cv.IPL_DEPTH_8U: 'uint8',
        cv.IPL_DEPTH_8S: 'int8',
        cv.IPL_DEPTH_16U: 'uint16',
        cv.IPL_DEPTH_16S: 'int16',
        cv.IPL_DEPTH_32S: 'int32',
        cv.IPL_DEPTH_32F: 'float32',
        cv.IPL_DEPTH_64F: 'float64',
    }

    arrdtype=im.depth
    a = np.fromstring(
         im.tostring(),
         dtype=depth2dtype[im.depth],
         count=im.width*im.height*im.nChannels)
    a.shape = (im.height,im.width,im.nChannels)
    return a

def array2cv(a):


    dtype2depth = {
            'uint8':   cv.IPL_DEPTH_8U,
            'int8':    cv.IPL_DEPTH_8S,
            'uint16':  cv.IPL_DEPTH_16U,
            'int16':   cv.IPL_DEPTH_16S,
            'int32':   cv.IPL_DEPTH_32S,
            'float32': cv.IPL_DEPTH_32F,
            'float64': cv.IPL_DEPTH_64F,
    }
    try:
        nChannels = a.shape[2]
    except:
        nChannels = 1
    
    cv_im = cv.CreateImageHeader((a.shape[1],a.shape[0]),
              dtype2depth[str(a.dtype)],
              nChannels)
    cv.SetData(cv_im, a.tostring(),
                 a.dtype.itemsize*nChannels*a.shape[1])
    return cv_im


server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
server_socket.bind(("", 15055))

print"UDPServer Waiting for client on port 15055"
x=0
framenum=0
im=''
data=''
a=''
#server_socket.setblocking(0)
cv.NamedWindow("w2", 1)
c = cv.WaitKey(10)
while 1:
	data, address = server_socket.recvfrom(50024)
	a=a+data
	print framenum
	framenum=framenum+1
	try:
		l=pickle.loads(a)
		a=''
		try:
			im=array2cv(l)
			print im
		except Exception, e:
			print 'failed to convert'
	except Exception, e:
		pass
		#print 'fail adding'
		#a=a+data
		#print len(a)

	if im!='':
		cv.ShowImage('w2',im)
		cv.WaitKey(10)

 #    try:
	#     data, address = server_socket.recvfrom(1100)
 #    except Exception, e:
	# 	print e
    
	# #print "( " ,address[0], " " , address[1] , " ) said : ", data
 #    if data == 'newframe':
 #    	#print data
 #        z=a
 #        a=''
 #        print framenum
 #        if len(z):
 #            try:
 #            	print z[:25]
 #            	print z[len(z)-25:25]
 #            	print len(z)
 #                l=pickle.loads(z)
 #            except Exception, e:
 #            	print len(z)
 #                print e
 #            try:
 #                im=array2cv(l)
 #            except Exception, e:
 #                print e

 #        framenum=framenum+1
        
 #        if im!='':
 #            cv.ShowImage("w2", im)
 #            c = cv.WaitKey(10)
 #            z=''
 #            a=''
 #    else:
 #        a=a+data
	
server_socket.close()